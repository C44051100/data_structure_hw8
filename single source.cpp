#include<iostream>
#include<string>
#include<vector>

using namespace std;

struct Node{
    int dest=-1;
    int cost=-1;
    int set=-1;
};

void shortest_path(vector<vector<Node>> a,int b){
    int path[100];
    int low_cost=10000;
    int low=0;
    int cur=0;
    int change=0;
    for(int i=0;i<100;i++)path[i]=-1;

    while(1){
        change=0;

        //set every node`s lowest cost path avaliable
        for(int i=0;i<a.size();i++){
            low_cost=a[i][0].cost;
            low=0;
            for(int j=1;j<a[i].size();j++){
                //cout<<a[i][j].cost<<" "<<low_cost;
                if(a[i][j].cost<low_cost && a[i][j].set!=1){
                    low_cost=a[i][j].cost;
                    low=j;
                    change=1;
                }
            }
            if(change==1)a[i][low].set=1;
        }

        //no path form 0 to destination
        if(change==0){
            cout<<"Impossible"<<endl;
            return;
        }

        //check if the path 0 to destination is avaliable
        while(1){
            //cout<<"uihruihf";
            int leave=0;
            int next=-1;
            int next_cost=1000;
            //int change_1=0;
            if(cur==b){
                int i=0;
                while(1){
                    if(path[i]==-1)break;
                    cout<<path[i]<<",";
                    i++;
                }
                cout<<cur<<endl;
                return;
            }

            for(int i=0;i<a[cur].size();i++){
                if(a[cur][i].set==1 && next_cost>a[cur][i].cost){
                    next=a[cur][i].dest;
                    next_cost=a[cur][i].cost;
                    //change_1=1;
                }
            }

            for(int i=0;i<100;i++){
                //cout<<path[i]<<endl;
                if(path[i]==next){
                    leave=1;
                    for(int i=0;i<100;i++)path[i]=-1;
                    break;
                }
                if(path[i]==-1){
                    path[i]=cur;
                    cur=next;
                    break;
                }
            }

            if(leave==1)break;

        }
    }
}

int main(){
    int vertex_num=0;
    int edge_num=0;

    int vert1=0;
    int vert2=0;
    int w=0;
    vector<vector<Node>> vertex;

    cin>>vertex_num>>edge_num;

    Node V_0;
    vector<Node> node_0(1,V_0);
    for(int i=0;i<vertex_num;i++){
        vertex.push_back(node_0);
    }

    for(int i=0;i<edge_num;i++){
        Node node_1;
        cin>>vert1>>vert2>>w;
        node_1.dest=vert2;
        node_1.cost=w;
        if(vertex[vert1][0].cost==-1){
            vertex[vert1][0].cost=w;
            vertex[vert1][0].dest=vert2;
        }
        else{
            vertex[vert1].push_back(node_1);
        }

        node_1.dest=vert1;
        if(vertex[vert2][0].cost==-1){
            vertex[vert2][0].cost=w;
            vertex[vert2][0].dest=vert1;
        }
        else{
            vertex[vert2].push_back(node_1);
        }
    }

    //cout<<vertex[0][1].set;

    for(int i=0;i<vertex_num;i++)shortest_path(vertex,i);

    return 0;
}
